{-# LANGUAGE GeneralizedNewtypeDeriving, TupleSections #-}

module GenTree where

import Control.Arrow
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.RWS
import Control.Monad.Trans.Maybe
import Data.List
import qualified Data.DList as DL
import Data.Maybe

import Graph

data Tree = Node Forest
          deriving (Eq, Ord, Show)
type Forest = [Tree]

treeSize :: Tree -> Int
treeSize (Node children) = (1+) . sum . map treeSize $ children

-- nice property: edges are represented as pairs (i,j) where i < j
treeToGraph :: Tree -> Graph
treeToGraph t = second (DL.toList) $ execRWS (f t) () 0
  where f :: Tree -> RWS () (DL.DList (Int, Int)) Int Int
        f (Node children) = do
          nodeId <- fresh
          childrenIds <- mapM f children
          tell . DL.fromList . map (nodeId,) $ childrenIds
          pure nodeId
        fresh = do
          x <- get
          put (x+1)
          pure x

randomTree :: Int -> Double -> IO Graph
randomTree targetSize eps = treeToGraph <$> randomTree' targetSize eps

randomTree' :: Int -> Double -> IO Tree
randomTree' targetSize eps =
  lcrs . fromJust <$> runGenM (2*targetSize+1) eps genTree



data BinTree = Nil | Cons BinTree BinTree
             deriving (Eq, Ord, Show)

-- left-child right-sibling transform
lcrs :: BinTree -> Tree
lcrs = Node . f
  where f Nil = []
        f (Cons car cdr) = lcrs car : f cdr

binTreeSize :: BinTree -> Int
binTreeSize Nil = 1
binTreeSize (Cons l r) = 1 + binTreeSize l + binTreeSize r

-- The following code is taken from
-- https://byorgey.wordpress.com/2013/04/25/random-binary-trees-with-a-size-limited-critical-boltzmann-sampler-2/

newtype GenM a = GenM 
    { unGenM :: ReaderT (Int,Int) (StateT Int (MaybeT (Rand StdGen))) a }
  deriving (Functor,
            Applicative, Alternative,
            Monad, MonadPlus,
            MonadRandom, MonadState Int, MonadReader (Int,Int))

runGenM :: Int -> Double -> GenM a -> IO (Maybe a)
runGenM targetSize eps m = do
  let wiggle  = floor $ fromIntegral targetSize * eps
      minSize = targetSize - wiggle
      maxSize = targetSize + wiggle
  g <- newStdGen
  return . (evalRand ?? g) . runMaybeT . (evalStateT ?? 0)
         . (runReaderT ?? (minSize, maxSize)) . unGenM
         $ m

genTreeUB :: GenM BinTree
genTreeUB = do
  r <- getRandom
  atom
  if r <= (1/2 :: Double)
    then return Nil
    else Cons <$> genTreeUB <*> genTreeUB

atom :: GenM ()
atom = do
  (_, maxSize) <- ask
  curSize <- get
  when (curSize >= maxSize) mzero
  put (curSize + 1)

genTreeLB :: GenM BinTree
genTreeLB = do
  put 0
  t <- genTreeUB
  tSize <- get
  (minSize, _) <- ask
  guard $ tSize >= minSize
  return t

genTree :: GenM BinTree
genTree = genTreeLB `mplus` genTree

-- Et ça, ça vient de Lens

infixl 1 ??
(??) :: Functor f => f (a -> b) -> a -> f b
fab ?? a = fmap ($ a) fab



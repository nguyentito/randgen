module Graph where

import Control.Applicative hiding (empty)
import Control.Monad
import Control.Monad.Random
import qualified Data.Vector as V
import Control.Arrow

type Graph = (Int, [(Int, Int)]) -- (size, sparse adjacency matrix)

graphNumVert, graphNumEdges :: Graph -> Int
graphNumVert = fst
graphNumEdges = length . snd

union :: Graph -> Graph -> Graph
union (s, e) (s', e') = (s+s', e ++ (map  (((+s)***(+s))) $ e'))

emptyGraph :: Graph
emptyGraph = (0, [])

printEdges :: Graph -> IO ()
printEdges = mapM_ (\(i, j) -> putStrLn $ show i ++ " " ++ show j) . snd

-- just change the ID numbers of the nodes and the order of edges to
-- "hide structure" and prevent some forms of cheating (e.g. computing
-- connected components as contiguous ranges of numbers)

shuffleGraph' :: Graph -> Rand StdGen Graph
shuffleGraph' (size, edges) = do
  subst <- V.fromList <$> permute [0..(size-1)]
  let renameNode =  (\(i,j) -> (subst V.! i, subst V.! j))
  shuffledEdges <- map renameNode <$> permute edges
  return (size, shuffledEdges)

shuffleGraph :: Graph -> IO Graph
shuffleGraph = evalRandIO . shuffleGraph'

-- purely functional shuffling ; taken from
-- http://apfelmus.nfshost.com/articles/random-permutations.html

-- List returning elements in random order
type RandomList a = Rand StdGen [a]

empty :: RandomList a
empty = return []

singleton :: a -> RandomList a
singleton x = return [x]

    -- Fair merge of random lists
merge :: RandomList a -> RandomList a -> RandomList a
merge rxs rys = do
        xs <- rxs
        ys <- rys
        merge' (length xs, xs) (length ys, ys)
    where
    merge' (0 , [])   (_ , ys)   = return ys
    merge' (_ , xs)   (0 , [])   = return xs
    merge' (nx, x:xs) (ny, y:ys) = do
        k <- getRandomR (1,nx+ny)   -- selection weighted by size
        if k <= nx
            then (x:) `liftM` ((nx-1, xs) `merge'` (ny, y:ys))
            else (y:) `liftM` ((nx, x:xs) `merge'` (ny-1, ys))

    -- Generate a random permutation in O(n log n)
permute :: [a] -> RandomList a
permute = f
    where f []  = empty
          f [x] = singleton x
          f xs  = (f l) `merge` (f r)
            where (l,r) = splitAt (length xs `div` 2) xs


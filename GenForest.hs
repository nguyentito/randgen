import GenTree
import Graph

forestParam :: [Int]
forestParam = [30000, 60000]

main :: IO ()
main = do
  trees <- mapM (flip randomTree 0.1) forestParam
  let forest' = foldr union emptyGraph trees
  forest <- shuffleGraph forest'
  putStrLn $ show (graphNumVert forest) ++ " " ++ show (graphNumEdges forest)
  printEdges forest
 


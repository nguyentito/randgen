import GenTree
import Graph

import Control.Monad.Random

forestParam :: [Int]
forestParam = [25, 50]

main :: IO ()
main = do
  trees <- mapM (flip randomTree' 0.1) forestParam
  let forest' = foldSpec mergeTreeWith (emptyGraph,[]) trees
 -- forest <- shuffleGraph forest'
  (forest,edgesAdded) <- forest'
  putStrLn $ show (graphNumVert forest) ++ " " ++ show (graphNumEdges forest) ++
									" " ++ show (300) --hardcoded, ugly
  printEdges forest

            
foldSpec :: (Tree -> Graph -> IO(Graph, ((Int,Int),(Int,Int)))) -> (Graph, [((Int,Int),(Int,Int))]) -> [Tree] -> IO(Graph, [((Int,Int),(Int,Int))])
foldSpec f (g,l) [a] = do
	return $ (treeToGraph a,[])
 
foldSpec f (g,l) (t:q) = do
  (a,b) <- f t g
  foldSpec f (a, b:l) q

          
            

mergeTreeWith :: Tree -> Graph -> IO (Graph,((Int,Int),(Int,Int)))
mergeTreeWith t g = do
  v1 <- getRandomR (0, fst g - 1) 
  v1' <- getRandomR (0, fst g -1) 
  v2 <- getRandomR (0, fst (treeToGraph t) - 1)
  v2' <- getRandomR (0, fst (treeToGraph t) - 1) --We should check
  return $ ((fst intermediate, (snd intermediate) ++ [(v1,fst g + v2),(v1',fst g + v2')]), ((v1,fst g+ v2),(v1',fst g + v2')))
            where  intermediate = union g (treeToGraph t)


import Data.Function
import Data.List

import GenTree
import Graph

treeParam :: Int
treeParam = 9000

main :: IO ()
main = do
  tree <- randomTree' treeParam 0.1
  let (numVert, treeEdges) = treeToGraph tree
  (_, treeCycleEdges) <- shuffleGraph (numVert, (leftmostLabel tree, 0) : treeEdges)
  putStrLn $ show numVert
  let ancestorList = map fst . sortBy (compare `on` snd) $ treeCycleEdges
  putStrLn . unwords . map show $ ancestorList
  let m1 = mis (truncateLeftmost tree)
      m2 = snd (mis' tree)
  putStrLn $ show (max m1 m2)
  
leftmostLabel :: Tree -> Int
leftmostLabel (Node []) = 0
leftmostLabel (Node (x:_)) = 1 + leftmostLabel x

truncateLeftmost (Node (Node []:xs)) = Node xs
truncateLeftmost (Node (x:xs)) = Node (truncateLeftmost x : xs)

rotateLeftmost (Node (Node []:xs)) = Node [Node xs]
rotateLeftmost (Node (x:xs)) =
  let (Node ys) = rotateLeftmost x in
  Node ((Node xs):ys)

mis' t = f t
  where f (Node xs) = let fs = map f xs in
                       (1 + sum (map snd fs), sum (map (uncurry max) fs))
mis t = let (inclusive, exclusive) = mis' t in max inclusive exclusive

